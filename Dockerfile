FROM ubuntu

RUN apt update
RUN apt install --yes python3 python3-pip

WORKDIR /opt/app

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY flask_project ./flask_project

ENV PYTHONPATH = '/opt/app/flask_project'

EXPOSE 5000

ENTRYPOINT python3 flask_project/main.py

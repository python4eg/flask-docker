from flask import Blueprint, render_template
from sqlalchemy import insert

from db import db
from models import BlogModel

base_route = Blueprint('base', __name__)

@base_route.route('/')
def root():
    insert_statement = insert(BlogModel).values(title='Lorem Ipsum 1', text='Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit')
    insert_statement2 = insert(BlogModel).values(title='Lorem Ipsum 2', text='Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit')
    insert_statement3 = insert(BlogModel).values(title='Lorem Ipsum 3', text='Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit')
    db.session.execute(insert_statement)
    db.session.execute(insert_statement2)
    db.session.execute(insert_statement3)
    db.session.commit()
    blogs = BlogModel.query.all()
    return render_template('blogs.html', blogs=blogs)
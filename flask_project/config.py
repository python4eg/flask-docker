import os


class FlaskConfig:
    SECRET_KEY = 'some_random_key'
    SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI', 'sqlite:///new.db?uri=true')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

create database raw;

create table if not exists blog_models(
    id serial not null primary key,
    title varchar(100) not null,
    text text not null
);

insert into blog_models(title, text)
values
('t1', 'description'),
('t2', 'description'),
('t3', 'description');